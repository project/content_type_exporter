
Content Type Exporter Instructions
**********************************

This suite of modules, based upon CCK's content_copy module, provides an
interface to export content type definitions in a way suitable for use with
Drupal's hook_type_info().
