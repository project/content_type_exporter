<?php

/**
 * @file
 * Create/Read/Update/Delete functions for CCK-defined object types.
 */

/**
 * Make changes needed when a content type is created.
 *
 * @param $info
 *   value supplied by hook_node_type()
 *
 * node_get_types() is still missing the new type at this point due to
 * a static caching bug. We ask it to rebuild its cache so that
 * content_clear_type_cache() can do its job properly.
 */
function content_type_loader_create($info) {
  node_get_types(NULL, NULL, TRUE);

  // Handle the variables.
  content_type_loader_set_module_vars($info);

  content_clear_type_cache(TRUE);
}

/**
 * Make changes needed when an existing content type is updated.
 *
 * @param $info
 *   value supplied by hook_node_type()
 */
function content_type_loader_update($info) {
  // Handle the variables.
  content_type_loader_set_module_vars($info);

  // Reset all content type info.
  // Menu needs to be rebuilt as well, but node types need to be rebuilt first.
  // node_type_form_submit() takes care of this.
  content_clear_type_cache(TRUE);
}

/**
 * Make changes needed when a content type is deleted.
 *
 * @param $info
 *   value supplied by hook_node_type()
 *
 * TODO should we skip doing this entirely since core leaves the
 * nodes in the database as orphans and wait until the nodes are
 * deleted to respond?
 *
 */
function content_type_loader_delete($info) {
  // Don't delete data for content-types defined by disabled modules.
  if (!empty($info->disabled)) {
    return;
  }

  // Handle the variables.
  content_type_loader_del_module_vars($info);

  // Menu needs to be rebuilt as well, but node types need to be rebuilt first.
  // node_type_form_submit() takes care of this.
  content_clear_type_cache(TRUE);
}

/**
 * Assign a set of variables for a module based on a defined list of known
 * settings.
 * @param $module
 *   The internal module name.
 * @param $settings
 *   The list of known variable names for this content type.
 * @param $info
 *   The content type's settings.
 */
function content_type_loader_set_module_vars($info) {
  // Obtain an array of all variables applicable for this content type.
  $variables = content_type_loader_get_all_variables($info->type);
  
  // Loop over the module settings array.
  foreach ($variables as $module => $setting) {
    // Does the setting exist in the content type definition?
    if (array_key_exists($setting, $info)) {
      // Is this module enabled?  Only do this check once.
      if ($x == 0 && !module_exists($module)) {
        drupal_set_message(t('Settings found for the @module module but it is not enabled.', array('@module' => $module)));
      }

      // See if the token value was added to the setting name.
      if (strpos($setting, CONTENT_HANDLE_VAR_TOKEN) !== FALSE) {
        $variable = str_replace(CONTENT_HANDLE_VAR_TOKEN, $info->type, $setting);
      }
      // The default is $setting_$type.
      else {
        $variable = $setting .'_'. $info->type;
      }

      // Assign the setting variable.
      variable_set($variable, $info->$setting);
    }
  }
}

/**
 * Remove a set of variables for a module based on a defined list of known
 * settings.
 * @param $module
 *   The internal module name.
 * @param $settings
 *   The list of known variable names for this content type.
 * @param $info
 *   The content type's settings.
 */
function content_type_del_module_vars($module, $info) {
  // Obtain an array of all variables applicable for this content type.
  $variables = content_type_loader_get_all_variables($info->type);

  // Loop over the module settings array.
  foreach ($variables as $module => $setting) {
    // See if the token value was added to the setting name.
    if (strpos($setting, CONTENT_HANDLE_VAR_TOKEN) !== FALSE) {
      $variable = str_replace(CONTENT_HANDLE_VAR_TOKEN, $info->type, $setting);
    }
    // The default is $setting_$type.
    else {
      $variable = $setting .'_'. $info->type;
    }
    // Remove the setting variable.
    variable_del($variable);
  }
}

/**
 * Obtain a nested array of all content type variables for a given type.
 *
 * @param $type
 *   The content type to build the variables list for.
 * @return
 *   The nested array of variables for this content type.
 */
function content_type_loader_get_all_variables($type) {
  // hook_content_type_variables($type)
  // Control addition and removal of variables related to content types using
  // via the hook_node_info() and hook_node_type() hooks. Elements are added
  // to the array $variables in the format: array('variable1', 'variable2');
  //
  // function mymodule_content_type_variables($type) {
  //   $variables = array(
  //     'variable1', // '_typename' will be added.
  //     'variable2', // '_typename' will be added.
  //     // CONTENT_HANDLE_VAR_TOKEN will be replaced with 'typename'.
  //     'var_'. CONTENT_HANDLE_VAR_TOKEN .'_able',
  //   );
  //   return $variables;
  // }

  $hook = 'content_type_variables';
  
  // Loop over each module implementation.
  foreach (module_implements($hook) as $module) {
    $function = $module .'_'. $hook;
    $result = call_user_func_array($function, $args);
    // If anything was found, add it to the array.
    if (!empty($result)) {
      $variables[$module] = $result;
    }
  }

  // hook_content_type_variables($type)
  // All the values to be altered.  Variable names are stored in the format:
  // $variables['modulename']['variablename'];
  drupal_alter('content_type_variables', $variables);
  
  return $variables;
}
