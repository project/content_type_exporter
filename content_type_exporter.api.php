
HOOKS
*****

/**
 * Control addition and removal of variables related to content types using
 * via the hook_node_info() and hook_node_type() hooks.
 *
 * @param $type
 *   The name of the content type being processed.
 * @return
 *   An array of variable names.
 */
function hook_content_type_variables($type) {
  $variables = array(
    'variable1', // '_typename' will be added.
    'variable2', // '_typename' will be added.
    // CONTENT_HANDLE_VAR_TOKEN will be replaced with 'typename'.
    'var_'. CONTENT_HANDLE_VAR_TOKEN .'_able',
  );
  return $variables;
}

/**
 * Override the values compiled in hook_content_type_variables().
 *
 * @param &$variables
 *   The array of variables.
 */
function hook_content_type_variables_alter(&$variables) {
  // Variable names are stored in the format:
  // $variables['modulename']['variablename'];
}

/**
 * Alter the content type settings array before it is displayed.
 *
 * @param &$export
 *   The array of content type settings.
 */
function hook_content_copy_export_alter(&$export) {
  // The $export array will contain two arrays:
  // * 'type' - the main content type settings.
  // * 'fields - the individual CCK fields.
}
